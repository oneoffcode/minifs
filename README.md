# minifs #

minifs is a small file server based on the basic http file server packages in the 
golang stdlib.

## Build

```
GO_ENABLED=0 go build minifs.go 
```

# Copyright #

Copyright (c) 2015, One Off Code LLC., All Righs Reserved

# LICENSE #

MIT unless otherwise stated by included libs.
