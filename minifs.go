package main

import (
	"crypto/md5"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"text/template"
	"time"
)

var (
	bind = flag.String("bind", ":8080", "server bind address and port")
	up   = flag.Bool("u", false, "upload only")
)

func maxAgeHandler(seconds int, h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("pragma", "no-cache")
		w.Header().Add("Cache-Control", fmt.Sprintf("max-age=%d, public, must-revalidate, proxy-revalidate, no-store, no-cache", seconds))
		h.ServeHTTP(w, r)
	})
}

var uploadform = `
<html>
<head>
    <title>Upload file</title>
</head>
<body>
<form enctype="multipart/form-data" action="upload" method="post">
      <input type="file" name="uploadfile" />
      <input type="hidden" name="token" value="{{.}}"/>
      <input type="submit" value="upload" />
</form>
</body>
</html>
`

// upload logic
func upload(w http.ResponseWriter, r *http.Request) {
	fmt.Println("method:", r.Method)
	if r.Method == "GET" {
		crutime := time.Now().Unix()
		h := md5.New()
		io.WriteString(h, strconv.FormatInt(crutime, 10))
		token := fmt.Sprintf("%x", h.Sum(nil))

		t := template.Must(template.New("upload").Parse(uploadform))
		t.Execute(w, token)
	} else {
		r.ParseMultipartForm(32 << 20)
		file, handler, err := r.FormFile("uploadfile")
		if err != nil {
			fmt.Println(err)
			return
		}
		defer file.Close()
		fmt.Fprintf(w, "%v", handler.Header)
		os.MkdirAll("./upload", 0770)
		f, err := os.OpenFile("./upload/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			fmt.Println(err)
			return
		}
		defer f.Close()
		io.Copy(f, file)
	}
}

func main() {
	flag.Parse()
	// Simple static webserver:
	http.HandleFunc("/upload", upload)
	http.Handle("/", maxAgeHandler(15, http.FileServer(http.Dir("."))))
	log.Fatal(http.ListenAndServe(*bind, nil))
}
